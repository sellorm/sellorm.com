tidy:
	tidy -i -w -m docs/index.html

build:
	nfpm package --packager deb --target /tmp/
